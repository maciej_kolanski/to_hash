#! /usr/bin/python3

import argparse
from os.path import basename, splitext, dirname, join
from os import rename
from hashlib import sha256


def prepare_parser():
    parser = argparse.ArgumentParser(prog='to_hash',
                                     description='Tool for renaming files to "sha256_hash_from_content.extension')
    parser.add_argument('files', nargs='+', help='List of files to rename')
    return parser.parse_args()


def main(args):
    files = args.files
    for file_path in files:
        with open(file_path, 'r') as f:
            hashed_content = sha256(f.read().encode()).hexdigest()
        new_name = hashed_content + splitext(basename(file_path))[1]
        rename(file_path, join(dirname(file_path), new_name))


if __name__ == '__main__':
    main(prepare_parser())
